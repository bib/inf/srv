{ config, pkgs, ... }:

{
  imports =
    [ # Include the results of the hardware scan.
      ./private/admins.nix
      ./services.nix
    ];
}

