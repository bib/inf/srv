# BIB service definitions
# =============================================================================
#
# This file describes the service configured on this server, and is a source
# of truth for other parts of the configuration. This should normally be the
# only file that needs to be edited when configuring new services.
#
# This file is used to generate :
#  - [DONE] SSL certificates (using ACME)
#  - [DONE] Reverse-proxy backend mappings (using haproxy)
#  - [DONE] Container interface definitions (using systemd)
#  - [TODO] NixOps container definitions (using nixops-lxd)
#  - [DONE] Private DNS records for each container (using /etc/hosts)
#  - [TODO] Public DNS records for each service (using PowerDNS)
#
# NOTE: See NixOS documentation's on `security.acme.certs` to know what options
#       can be used in the `certificate` option.
#
# NOTE: Certificate using a web challenge should use the webroot provided by
#       `config.security.acme.webroot`.
#
# Documentation
# -----------------------------------------------------------------------------
#
# In order to launch a new service with its own container :
#  - make sure the service is defined in the appropriate backend list, with 
#    its domain name (in `match`) and required backend address (in `address`)
#  - run "nixos-rebuild switch" to switch to the new configuration
#  - run "bib-launch-service <NAME>" to launch the container

{ config ? {}, ... }:

{

  # General settings
  # +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

  deployment.externalIPv4 = "192.168.255.202";
  deployment.internalIPv4 = "10.13.255.222";
  #deployment.publicIPv6 = "...";

  # Deployed services
  # +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

  deployment.services = {

    # "BIB" services
    # ---------------------------------------------------------------------------

    "lebib.org" = {
      # ACME options (for certificate renewal)
      certificate = {
        # Contact address for the generated certificate
        email = "contact@lebib.org";
        # Parameters for DNS-01 challenge
        dnsProvider = "gandiv5";
        credentialsFile = "/var/secrets/pstch-gandi-api-credentials";
        # Use staging server (NOTE: comment out to set to production)
        # server = "https://acme-staging-v02.api.letsencrypt.org/directory";
        # Extra domain names (not bound to a backend server)
        extraDomainNames = [ "bibix.lebib.org" ];
      };
      # Use wildcard certificate
      wildcard = true;
      # HTTP backend definitions (used for haproxy)
      backends = {
        "base-debian" = {
          address = "10.13.222.255";
        }; 
        "base-nixos" = {
          address = "10.13.222.254";
        }; 
        "bib-nfscold" = {
          address = "10.13.222.1";
        }; 
        "bib-nfshot" = {
          address = "10.13.222.2";
        }; 
      };
    };
  };
}

