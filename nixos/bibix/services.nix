# BIB service definitions
# =============================================================================
#
# This file describes the service configured on this server, and is a source
# of truth for other parts of the configuration. This should normally be the
# only file that needs to be edited when configuring new services.
#
# This file is used to generate :
#  - [DONE] SSL certificates (using ACME)
#  - [DONE] Reverse-proxy backend mappings (using haproxy)
#  - [DONE] Container interface definitions (using systemd)
#  - [TODO] NixOps container definitions (using nixops-lxd)
#  - [DONE] Private DNS records for each container (using /etc/hosts)
#  - [TODO] Public DNS records for each service (using PowerDNS)
#
# NOTE: See NixOS documentation's on `security.acme.certs` to know what options
#       can be used in the `certificate` option.
#
# NOTE: Certificate using a web challenge should use the webroot provided by
#       `config.security.acme.webroot`.
#
# Documentation
# -----------------------------------------------------------------------------
#
# In order to launch a new service with its own container :
#  - make sure the service is defined in the appropriate backend list, with 
#    its domain name (in `match`) and required backend address (in `address`).
#    The backend's name must length 11 char max
#  - run "nixos-rebuild switch" to switch to the new configuration
#  - run "bib-launch-service <NAME>" to launch the container

{ config ? {}, ... }:

{

  # General settings
  # +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

  deployment.externalIPv4 = "163.172.231.232";
  deployment.internalIPv4 = "10.13.255.208";
  #deployment.publicIPv6 = "...";

  # Deployed services
  # +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

  deployment.services = {

    # "BIB" services
    # ---------------------------------------------------------------------------

    "lebib.org" = {
      # ACME options (for certificate renewal)
      certificate = {
        # Contact address for the generated certificate
        email = "contact@lebib.org";
        # Parameters for DNS-01 challenge
        dnsProvider = "gandiv5";
        credentialsFile = "/var/secrets/pstch-gandi-api-credentials";
        # Use staging server (NOTE: comment out to set to production)
        # server = "https://acme-staging-v02.api.letsencrypt.org/directory";
        # Extra domain names (not bound to a backend server)
        extraDomainNames = [ "bibix.lebib.org" ];
      };
      # Use wildcard certificate
      wildcard = true;
      # HTTP backend definitions (used for haproxy)
      backends = {
        "base-debian" = {
          address = "10.13.209.255";
        };
        "base-nixos" = {
          address = "10.13.209.254";
        };
        "bib-drupal" = {
          domains = [ "migration.lebib.org" ];
          address = "10.13.208.1";
          http.enable = true;
        };
        "bib-auth" = {
          enable = false;
          domains = [ "auth.lebib.org" ];
          address = "10.13.208.2";
          http.enable = true;
        };
        "bib-mail" = {
          domains = [ "mx0.lebib.org" ];
          address = "10.13.208.3";
          http.enable = true;
          mail.enable = true;
        };
        "bib-webmail" = {
          enable = false;
          domains = [ "webmail.lebib.org" ];
          address = "10.13.208.4";
          http.enable = true;
        };
        "bib-lists" = {
          enable = false;
          domains = [ "lists.lebib.org" ];
          address = "10.13.208.5";
          http.enable = true;
        };
        "bib-cloud" = {
          domains = [ "cloud.lebib.org" ];
          address = "10.13.208.6";
          http.enable = true;
          http.config.option.forwardfor = true;
	  # timeouts for push notif
          http.config.timeout.server = "10m";
          http.config.timeout.client = "10m";
	  http.config.timeout.connect = "10m";
	  http.config.no.option.http-buffer-request = false;
        };
        "bib-admin" = {
          domains = [ "dolibarr.lebib.org" ];
          address = "10.13.208.7";
          http.enable = true;
        };
        "bib-paheko" = {
          domains = [ "paheko.lebib.org" ];
          address = "10.13.208.23";
          http.enable = true;
        };

        "bib-stream" = {
          domains = [ "stream.lebib.org" ];
          address = "10.13.208.8";
          http.enable = true;
        };
        "bib-code" = {
          enable = false;
          domains = [ "code.lebib.org" ];
          address = "10.13.208.9";
          http.enable = true;
        };
        "bib-docu" = {
          enable = false;
          domains = [ "docu.lebib.org" ];
          address = "10.13.208.10";
          http.enable = true;
        };
        "bib-labase" = {
          enable = false;
          domains = [ "labase.lebib.org" ];
          address = "10.13.208.11";
          http.enable = true;
        };
        "bib-labim" = {
          domains = [ "labim.lebib.org" ];
          address = "10.13.208.12";
          http.enable = true;
        };
        "bib-tryton" = {
          domains = [ "tryton.lebib.org" ];
          address = "10.13.208.13";
          http.enable = true;
        };
        "bib-elab" = {
          domains = [ "elab.lebib.org" ];
          address = "10.13.208.14";
          http.enable = true;
        };
        "bib-zapa" = { 
          domains = [ "zapatista2021.lebib.org" ];
          address = "10.13.208.15";
          http.enable = true;
        };
        "bib-cafe" = {
          domains = [ "cafe.lebib.org" ];
          address = "10.13.208.16";
          http.enable = true;
        };
        "bib-brew" = {
          domains = ["brew.lebib.org"];
          address = "10.13.208.17";
          http.enable = true;
        };

        "bib-opcon" = {
          domains = ["concerto.lebib.org"];
          address = "10.13.208.18";
        };
        "bib-rcm" = {
          domains = ["rcm.lebib.org"];
          address = "10.13.208.19";
          http.enable = true;
        };

        "bib-pretalx" = {
          domains = ["pretalx.lebib.org"];
          address = "10.13.208.20";
          http.enable = true;
        };

        "bib-forum" = {
          domains = ["forum.lebib.org"];
          address = "10.13.208.21";
          http.enable = true;
        };

        "bib-chorale" = {
          domains = ["choeurdelutte.lebib.org"];
          address = "10.13.208.22";
          http.enable = true;
        };

        "bib-website" = {
          domains = ["new.lebib.org"];
          address = "10.13.208.24";
          http.enable = true;
        };
        "bib-l2m2p" = {
          domains = ["l2m2p.lebib.org"];
          address = "10.13.208.25";
          http.enable = true;
	  http.config.option.forwardfor = true;
        };
#	"bib-tetrinet" = {
#	  domains = [ "tetrinet.lebib.org" ];
#	  address = "10.13.208.16";
#	  http.enable = true;
#	};
        "bib-bla" = { # mattermost
          domains = ["bla.lebib.org"];
          address = "10.13.208.26";
          http.enable = true;
        };
        "core-mgmt" = {
          domains = [ "mgmt.lebib.org" ];
          address = "10.13.240.240";
          http.enable = true;
        };
        "core-sql" = {
          address = "10.13.240.36";
        };
        "core-ldap" = {
          address = "10.13.240.89";
        };
        "core-fdir" = {
          address = "10.13.240.90";
        };
        "core-llng" = {
          domains = [ "auth.lebib.lan" "reload.lebib.lan" "manager.lebib.lan" ];
          address = "10.13.240.91";
        };
      };
    };

    # "La Tendresse" services
    # ---------------------------------------------------------------------------

    "latendresse.fr" = {

      # ACME options (for certificate renewal)
      certificate = {
        # Contact address for the generated certificate
        email = "contact@lebib.org";
        # Use a webroot challenge
        webroot = config.security.acme.webroot or null;
      };
      # HTTP backend definitions (used for haproxy)
      backends = {
        "tnd-spip" = {
          domains = [ "www.latendresse.fr" "latendresse.fr" "new.latendresse.fr" ];
          address = "10.13.208.33";
          http.enable = true;
        };
        "tnd-cloud" = {
          domains = [ "cloud.latendresse.fr" ];
          address = "10.13.208.34";
          http.enable = true;
          http.config.option.forwardfor = true;
          # timeouts for push notif
          http.config.timeout.server = "10m";
          http.config.timeout.client = "10m";
          http.config.timeout.connect = "10m";
          http.config.no.option.http-buffer-request = false;
        };
        "tnd-admin" = {
          domains = [ "pepettes.latendresse.fr" ];
          address = "10.13.208.35";
          http.enable = true;
        };
#	"tnd-tibillet" = {
#	  domains = [ "tibillet.latendresse.fr" ];
#	  address = "10.13.208.36";
#	  http.enable = true;
#	};
      };
    };

    # Externals services
    # ---------------------------------------------------------------------------

    "latenaille.org" = {
      # ACME options (for certificate renewal)
      certificate = {
        # Contact address for the generated certificate
        email = "latenaille-contact@lebib.org";
        # Parameters for DNS-01 challenge
        dnsProvider = "gandiv5";
        credentialsFile = "/var/secrets/pstch-gandi-api-credentials";
        # Use staging server (NOTE: comment out to set to production)
        # server = "https://acme-staging-v02.api.letsencrypt.org/directory";
      };
      # Use wildcard certificate
      wildcard = true;
      # HTTP backend definitions (used for haproxy)
      backends = {
        "ext-tnll" = {
          domains = ["latenaille.org" "www.latenaille.org"];
          address = "10.13.208.200";
          http.enable = true;
        };
      };
    };

    "dadpip.institute" = {
      # ACME options (for certificate renewal)
      certificate = {
        # Contact address for the generated certificate
        email = "wargreen@lebib.org";
        # Use a webroot challenge
        webroot = config.security.acme.webroot or null;
      };
      # HTTP backend definitions (used for haproxy)
      backends = {
	"ext-dadpip" = {
	  domains = ["dadpip.institute"];
	  address = "10.13.208.100";
	  http.enable = true;
	};
      };
    };

    "aleale.org" = {
      certificate = {
        email = "ale@lebib.org";
        dnsProvider = "gandiv5";
        credentialsFile = "/var/secrets/pstch-gandi-api-credentials";
      };
      backends = {
        "ext-ale" = {
          domains = ["aleale.org" "www.aleale.org"];
          address = "10.13.208.101";
          http.enable = true;
        };
      };
    };
  };
}
