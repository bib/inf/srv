# bibix.lebib.org -- main configuration file
# ============================================================================

{ config, pkgs, lib, ... }:

{
  imports = [
    # System modules
    # -------------------------------------------------------------------------

    ./private/admins.nix           # Admin accounts definitions
    ./private/environments.nix     # Deployment environments definitions 

    # Services 
    # -------------------------------------------------------------------------

    ./services.nix                 # Services definitions

  ];

}
