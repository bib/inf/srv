{
  description = "Configuration privée de l'infrastructure du BIB";

  inputs = {};

  outputs = inputs:
  {
    nixosModules = {
      alambix = ./nixos/alambix/configuration.nix;
      bibix = ./nixos/bibix/configuration.nix;
    };
  };
}

